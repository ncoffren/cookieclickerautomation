﻿namespace CookieClickerPlayer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CookieClicker = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // CookieClicker
            // 
            this.CookieClicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CookieClicker.Location = new System.Drawing.Point(0, 0);
            this.CookieClicker.MinimumSize = new System.Drawing.Size(20, 20);
            this.CookieClicker.Name = "CookieClicker";
            this.CookieClicker.ScriptErrorsSuppressed = true;
            this.CookieClicker.Size = new System.Drawing.Size(903, 480);
            this.CookieClicker.TabIndex = 0;
            this.CookieClicker.Url = new System.Uri("http://orteil.dashnet.org/cookieclicker/", System.UriKind.Absolute);
            this.CookieClicker.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.CookieClicker_DocumentCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 480);
            this.Controls.Add(this.CookieClicker);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += Form1_FormClosing;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser CookieClicker;
    }
}

