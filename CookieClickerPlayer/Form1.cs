﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CookieClickerPlayer
{
    public partial class Form1 : Form
    {
        private const int ClicksPerSecond = 20;
        private bool isStarted = false;
        private Thread ClickerThread;
        public Form1()
        {
            InitializeComponent();
            //add a registry key
            SetIE8KeyforWebBrowserControl(Process.GetCurrentProcess().ProcessName + ".exe");
            CookieClicker.Url = new Uri("http://orteil.dashnet.org/cookieclicker/");
        }

        private void CookieClicker_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WebBrowser wb = (WebBrowser)sender;
            HtmlDocument doc = wb.Document;
            ClickerThread = new Thread(() => Clicker());
            var cookie = CookieClicker.Document.GetElementsByTagName("div");
            if (cookie.Count > 0 && ClickerThread.ThreadState != System.Threading.ThreadState.Running && !isStarted)
            {
                //search for a bigCookie
                for (var x = 0; x < cookie.Count - 1; x++)
                {
                    //Console.WriteLine("Element: " + cookie[x].Id);
                    if (cookie[x].Id == "bigCookie")
                    {
                        //found the big cookie, so we can start the game
                        ClickerThread.Start();
                    }
                }
            }
            
        }

        //click automate
        private void Clicker()
        {
            HtmlDocument doc = GetDocument();
            isStarted = true;
            try
            {
                //doc.GetElementById("prefsButton").Focus();
                //doc.GetElementById("prefsButton").InvokeMember("click");
                ////disable options
                //foreach (HtmlElement o in doc.GetElementById("menu").Children)
                //{
                //    if (o.GetAttribute("className") == "listing")
                //    {
                //        foreach(HtmlElement a in o.Children)
                //        {
                //            if (!a.GetAttribute("className").Contains("off"))
                //            {
                //                a.InvokeMember("click");
                //            }
                //        }
                //    }
                //}
                while (true)
                {
                    bool tookAction = false;
                    try
                    {
                        if (doc.GetElementById("upgrades").Children.Count > 0)
                        {
                            //check upgrades available
                            foreach (HtmlElement child in doc.GetElementById("upgrades").Children)
                            {
                                if (child.GetAttribute("className").Contains("enabled"))
                                {
                                    child.InvokeMember("click");
                                    tookAction = true;
                                    break;
                                }
                            }
                        }
                        //autobuy all the things
                        HtmlElement buyer = null;
                        double highestEff = 0.00;
                        foreach (HtmlElement child in doc.GetElementById("products").Children)
                        {
                            if (child.GetAttribute("className").Contains("unlocked"))
                            {
                                //mouse over the element to get some text we'll use
                                child.InvokeMember("onmouseover");
                                //get the tooltip element
                                string data = doc.GetElementById("tooltip").InnerHtml;
                                //parse the data to get the per second string value - if there's no data, we haven't bought one so this should be priority
                                if (!data.Contains("<b>"))
                                {
                                    buyer = child;
                                    highestEff = 1;
                                }
                                else
                                {
                                    string perSecond = data.Substring(data.IndexOf("<b>") + "<b>".Length, data.IndexOf("</b>") - data.IndexOf("<b>") - "<b>".Length);
                                    double actual = Convert.ToDouble(perSecond);
                                    //how many do we have?
                                    string productId = child.Id.Substring(child.Id.Length - 1, 1);
                                    HtmlElement priceElement = doc.GetElementById("productPrice" + productId);
                                    double price = Convert.ToDouble(priceElement.InnerHtml);
                                    //calculate effiency - does it give the most cookies per second for its price?
                                    double eff = actual / price;
                                    Console.WriteLine(string.Format("{0}: Eff: {1} Max: {2}", child.Id, eff, highestEff));
                                    if (eff > highestEff)
                                    {
                                        buyer = child;
                                        highestEff = eff;
                                    }
                                }
                            }
                        }
                        if (buyer != null && !tookAction)
                        {
                            //if disabled, we want to skip this element until we can afford to buy it
                            if (!buyer.GetAttribute("className").Contains("disabled"))
                            {
                                buyer.InvokeMember("click");
                                tookAction = true;
                            }
                        }
                        if (!tookAction)
                            doc.GetElementById("bigCookie").InvokeMember("click");

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("exception!");
                        //hack to close the program when the thread can't find the main form - TODO: make this more sane
                        if (e.HResult == -2146233049)
                        {
                            Environment.Exit(0);
                        }
                    }

                    Thread.Sleep(1000 / ClicksPerSecond);
                }
            } catch { Thread.Sleep(1000); Clicker(); }
        }
        private void SetIE8KeyforWebBrowserControl(string appName)
        {
            RegistryKey Regkey = null;
            try
            {
                Regkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);
                if (string.IsNullOrEmpty(Convert.ToString(Regkey.GetValue(appName))))
                {
                    Regkey.SetValue(appName, unchecked((int)0x270F), RegistryValueKind.DWord);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Application Settings Failed");
                MessageBox.Show(ex.Message);
            }
            finally
            {
                // Close the Registry
                if (Regkey != null)
                    Regkey.Close();
            }
        }

        //threading help
        public delegate string GetStringHandler();
        public string GetDocumentText()
        {
            if (InvokeRequired)
                return Invoke(new GetStringHandler(GetDocumentText)) as string;
            else
                return CookieClicker.DocumentText;
        }

        public delegate HtmlDocument GetHtmlDocument();

        public HtmlDocument GetDocument()
        {
            if (InvokeRequired)
                return Invoke(new GetHtmlDocument(GetDocument)) as HtmlDocument;
            else
                return CookieClicker.Document;
        }

        private void Form1_FormClosing(object sender, EventArgs e)
        {
            ClickerThread.Abort();
        }
    }
}
